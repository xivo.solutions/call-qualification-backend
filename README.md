Call Qualification Backend
==========================

This project handles qualification request to xuc server and is closely linked to call qualification frontend one.

Installation
------------

To be able to deploy the qualification, please refer to call-qualification-frontend project.

Run on local
-------------

to run the sources on your local pc, run the command :
```bash
sbt run -Dxivohost=ip-where-your-xivo-is -Dxuc.host=ip-where-your-xuc-is -Dhttp.port=9900 -DfrontEndBaseUrl=https://where-the-front-is/call-qualification/
```


if you want the app to run with a local Xucmgt, add the flag on the Xucmgt command
```bash
-Dagent.thirdPartyWsUrl=http://localhost:9900/thirdparty/ws
```
