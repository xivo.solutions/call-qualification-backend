package configuration

import javax.inject.Inject
import play.api.Configuration

class AppConfig @Inject()(implicit val config: Configuration) {
  def xucHost: String = config.get[String]("xuc.host")
  def xucPort: Option[String] = config.getOptional[String]("xuc.port")
  def baseUrl: String = config.get[String]("frontEndBaseUrl")
}
