package model

import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, Json, Reads, Writes}


case class WsUser(userId: Long, agentId: Long, firstName: String, lastName: String, fullName: String, token: String)

case class WsQueue(id: Long, name: String, displayName: String, number: String)

case class WsCall(linkedId: String, uniqueId: String)

case class WsRequest(callee: String, caller: String, user: WsUser, queue: WsQueue, userData: Option[Map[String, String]],
                     callDataCallId: String)

object WsRequest {
  implicit val wsQueueReads: Reads[WsQueue] = (
    (JsPath \ "id").read[Long] and
      (JsPath \ "name").read[String] and
      (JsPath \ "displayName").read[String] and
      (JsPath \ "number").read[String]
    ) (WsQueue.apply _)

  implicit val wsQueueWrites = new Writes[WsQueue] {
    def writes(o: WsQueue) = Json.obj(
      "id" -> o.id,
      "name" -> o.name,
      "displayName" -> o.displayName,
      "number" -> o.number
    )
  }

  implicit val wsUserReads: Reads[WsUser] = (
    (JsPath \ "userId").read[Long] and
      (JsPath \ "agentId").read[Long] and
      (JsPath \ "firstName").read[String] and
      (JsPath \ "lastName").read[String] and
      (JsPath \ "fullName").read[String] and
      (JsPath \ "token").read[String]
    ) (WsUser.apply _)

  implicit val wsUserWrites = new Writes[WsUser] {
    def writes(o: WsUser) = Json.obj(
      "userId" -> o.userId,
      "agentId" -> o.agentId,
      "firstName" -> o.firstName,
      "lastName" -> o.lastName,
      "fullName" -> o.fullName
    )
  }

  implicit val wsRequestReads: Reads[WsRequest] = (
    (JsPath \ "callee").read[String] and
      (JsPath \ "caller").read[String] and
      (JsPath \ "user").read[WsUser] and
      (JsPath \ "queue").read[WsQueue] and
      (JsPath \ "userData").readNullable[Map[String, String]] and
      (JsPath \ "callDataCallId").read[String]
    ) (WsRequest.apply _)

  implicit val wsRequestWrites = new Writes[WsRequest] {
    def writes(o: WsRequest) = Json.obj(
      "callee" -> o.callee,
      "caller" -> o.caller,
      "user" -> o.user,
      "queue" -> o.queue,
      "userData" -> o.userData
    )
  }
}
