package model

import play.api.libs.json.{JsPath, Writes}
import play.api.libs.functional.syntax._

case class WsResponse(action:String, event:Option[String] = None, url: Option[String] = None,
                      autopause:Boolean = false, title:Option[String] = None, autopauseReason: Option[String] = None)

object WsResponse {
  implicit val wsResponseWrites: Writes[WsResponse] = (
    (JsPath \ "action").write[String] and
    (JsPath \ "event").writeNullable[String] and
    (JsPath \ "url").writeNullable[String] and
    (JsPath \ "autopause").write[Boolean] and
    (JsPath \ "title").writeNullable[String] and
    (JsPath \ "autopauseReason").writeNullable[String]
  )(unlift(WsResponse.unapply))
}
