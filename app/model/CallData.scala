package model

import org.joda.time.DateTime
import play.api.libs.functional.syntax._
import play.api.libs.json._

case class CallData(caller: String, callee: String, agentId: Int, queueId: Int, callId: String, token: String)

object CallData {
  implicit val callDataWrites: Writes[CallData] {
    def writes(callData: CallData): JsObject
  } = new Writes[CallData] {
    def writes(callData: CallData) = Json.obj(
      "caller" -> callData.caller,
      "callee" -> callData.callee,
      "agentId" -> callData.agentId,
      "queueId" -> callData.queueId,
      "callId" -> callData.callId,
      "token" -> callData.token
    )
  }
}

case class CallDataSubmit(subQualificationId: Int, callid: String, firstName: Option[String], lastName: Option[String],
                          agentId: Int, queueId: Int, comment: Option[String],  customData: Option[JsObject],
                          token: String)

object CallDataSubmit {
  implicit val callDataSubmitWrites: Writes[CallDataSubmit] {
    def writes(callDataSubmit: CallDataSubmit): JsObject
  } = new Writes[CallDataSubmit] {
    def writes(callDataSubmit: CallDataSubmit) = {
      Json.obj(
        "sub_qualification_id" -> callDataSubmit.subQualificationId,
        "time" -> dateTimeNow,
        "callid" -> callDataSubmit.callid,
        "agent" -> callDataSubmit.agentId,
        "queue" -> callDataSubmit.queueId,
        "first_name" -> callDataSubmit.firstName.getOrElse("").toString,
        "last_name" -> callDataSubmit.lastName.getOrElse("").toString,
        "comment" -> callDataSubmit.comment.getOrElse("").toString,
        "custom_data" -> callDataSubmit.customData.getOrElse("").toString
      )
    }
  }

  implicit val callDataSubmitReads: Reads[CallDataSubmit] = (
    (JsPath \ "sub_qualification_id").read[Int] and
    (JsPath \ "callid").read[String] and
      (JsPath \ "first_name").readNullable[String] and
      (JsPath \ "last_name").readNullable[String] and
      (JsPath \ "agent").read[Int] and
      (JsPath \ "queue").read[Int] and
      (JsPath \ "comment").readNullable[String] and
      (JsPath \ "custom_data").readNullable[JsObject] and
      (JsPath \ "token").read[String]
    )(CallDataSubmit.apply _)

  def dateTimeNow: String = {
    val format = "yyyy-MM-dd HH:mm:ss"
    DateTime.now().toString(format)
  }
}