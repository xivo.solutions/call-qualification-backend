package controllers.helper

import configuration.AppConfig
import javax.inject.Inject
import play.api.libs.ws.{WSClient, WSRequest}

class XucWs @Inject() (ws: WSClient, config: AppConfig) {

  def getXucUrl(resource: String, protocol: String = "http") =
    s"$protocol://${config.xucHost}${config.xucPort.map(":" + _).getOrElse("")}/xuc/api/2.0/$resource"

  def request(resource: String, method: String, token: String): WSRequest = {
    ws.url(getXucUrl(resource))
      .withQueryStringParameters(("token", token))
      .withMethod(method)
  }
}
