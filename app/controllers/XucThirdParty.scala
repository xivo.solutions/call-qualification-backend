package controllers

import akka.event.slf4j.Logger
import controllers.helper.XucWs

import javax.inject.Inject
import model.{WsRequest, WsResponse}
import play.api.Configuration
import play.api.libs.json.{JsError, JsSuccess, Json}
import play.api.mvc._

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

class XucThirdParty @Inject()(configuration: Configuration, requester: XucRequester, xucWs: XucWs) extends InjectedController {

  val logger = Logger(getClass.getName)
  val baseurl: String = configuration.get[String]("frontEndBaseUrl")
  val serviceName = "XucThirdParty"
  val autopauseReason: Option[String] = configuration.getOptional[String]("autopauseReason")
  val event: Option[String] = configuration.getOptional[String]("event") 
 
  private def getAutoPauseReason(r: WsRequest): Option[String] = {
    configuration
      .getOptional[String]("autopauseReasonPerQueue")
      .map(reasons => Json
        .parse(reasons)
        .validate[Map[String, String]]
      )
      .collect {
        case JsSuccess(value, _) => value
      } flatMap (_.get(r.queue.name)) orElse configuration.getOptional[String]("autopauseReason")
  }

  def logRequest(requestAction: String): Unit = {
    logger.info(s"Req: <$serviceName - $requestAction>")
  }

  def handleException() = PartialFunction[Throwable, Result] {
    case e: Exception =>
      logger.error(s"Error while processing the request: ${e.getMessage}")
      Results.InternalServerError(e.toString).withHeaders(ACCESS_CONTROL_ALLOW_ORIGIN -> "*")
  }

  def createOnCallUrl(r: WsRequest): Option[String] = {
    val res = Some(baseurl +
      "/qualification?queueId=" + r.queue.id +
      "&token=" + r.user.token +
      "&caller=" + r.caller +
      "&callee=" + r.userData.get.getOrElse("USR_XIVO_DSTNUM", r.callee)+
      "&agentId=" + r.user.agentId +
      "&queueId=" + r.queue.id +
      "&callId=" + r.callDataCallId +
      "&xucUrl=" + configuration.getOptional[String]("edge.host")
                   .getOrElse(configuration.get[String]("xuc.host"))
    )
    logger.debug(s"$r converted to url: $res")
    res
  }

  def preflight = Action { implicit request =>
    Ok("").withHeaders(
      ACCESS_CONTROL_ALLOW_ORIGIN -> "*",
      ACCESS_CONTROL_ALLOW_METHODS -> "POST, OPTIONS",
      ACCESS_CONTROL_ALLOW_HEADERS -> "Origin, X-Requested-With, Content-Type, Accept"
    )
  }

  def onCall = Action.async { implicit request =>
    val requestAction = "POST"
    logRequest(requestAction)

    request.body.asJson match {
      case None =>
        Future.successful(BadRequest("No JSON found").withHeaders(ACCESS_CONTROL_ALLOW_ORIGIN -> "*"))
      case Some(json) =>
        json.validate[WsRequest] match {
          case JsSuccess(r, _) =>
            requester.getQualifications(r.queue.id, r.user.token)
              .map { response =>
                if (response.isEmpty) {
                  logger.debug(s"Queue id: ${r.queue.id} has no qualifications assigned.")
                  NotFound(s"Queue id: ${r.queue.id} has no qualifications assigned.").withHeaders(ACCESS_CONTROL_ALLOW_ORIGIN -> "*")
                }
                else {
                  val pause = getAutoPauseReason(r) 
                  Ok(Json.toJson(WsResponse("open", event , createOnCallUrl(r),
                    pause.nonEmpty, Some("Qualification"), pause))).withHeaders(ACCESS_CONTROL_ALLOW_ORIGIN -> "*")
                }
              }
              .recover(handleException())
          case JsError(_) =>
            Future.successful(BadRequest("JSON is not conforming the request format")
              .withHeaders(ACCESS_CONTROL_ALLOW_ORIGIN -> "*"))
        }
    }
  }

}
