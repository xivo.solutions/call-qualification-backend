package controllers

import javax.inject._
import play.api.mvc._
import configuration.AppConfig

@Singleton
class IndexController @Inject()(config: AppConfig) extends InjectedController {

  def index = Action { implicit request => Ok(views.html.index("call qualification backend", config)).withHeaders(ACCESS_CONTROL_ALLOW_ORIGIN -> "*") }

}
