package controllers

import akka.event.slf4j.Logger
import javax.inject.Inject
import model.CallDataSubmit
import play.api.Configuration
import play.api.libs.json.{JsError, JsSuccess}
import play.api.mvc.{InjectedController, Result, Results}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class Application @Inject() (configuration: Configuration, requester: XucRequester) extends InjectedController {

  val logger = Logger(getClass.getName)
  val serviceName = "Application"

  def logRequest(requestAction: String): Unit = {
    logger.info(s"Req: <$serviceName - $requestAction>")
  }

  def handleException(requestAction: String) = PartialFunction[Throwable, Result] {
    case e: Exception =>
      logger.error(s"Error while processing the request: ${e.getMessage}")
      Results.InternalServerError(e.toString).withHeaders(ACCESS_CONTROL_ALLOW_ORIGIN -> "*")
    case f =>
      logger.error(s"Error while processing the request: ${f.getMessage}")
      Results.InternalServerError(f.toString).withHeaders(ACCESS_CONTROL_ALLOW_ORIGIN -> "*")
  }

  def submit()= Action.async { request =>
    val requestAction = "POST"
    logRequest(requestAction)

    request.body.asJson match {
      case None =>
        Future.successful(BadRequest("No JSON found").withHeaders(ACCESS_CONTROL_ALLOW_ORIGIN -> "*"))
      case Some(json) =>
        json.validate[CallDataSubmit] match {
          case JsSuccess(r, _) =>
            requester.saveQualification(r, r.token)
              .map(_ => NoContent.withHeaders(ACCESS_CONTROL_ALLOW_ORIGIN -> "*"))
              .recover(handleException(requestAction))
          case JsError(e) =>
            Future.successful(BadRequest("JSON is not conforming the request format").withHeaders(ACCESS_CONTROL_ALLOW_ORIGIN -> "*"))
        }
    }
  }

}
