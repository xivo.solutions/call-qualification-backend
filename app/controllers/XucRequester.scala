package controllers

import controllers.helper.XucWs
import javax.inject.Inject
import model._
import org.slf4j.{Logger, LoggerFactory}
import play.api.libs.json.{JsError, JsSuccess, Json, Reads}
import play.api.libs.ws._
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

class XucRequester @Inject() (xucWs: XucWs) {
  val logger: Logger = LoggerFactory.getLogger(getClass)

  def getResponseFromJson[T](jsonResponse: WSResponse)(implicit reads: Reads[T]): T = {
    jsonResponse.json.validate[T] match {
      case response: JsSuccess[T] =>
        response.get
      case e: JsError =>
        logger.error(s"Could not read from xuc server, $e")
        throw new Exception("Non understandable JSON returned")
    }
  }

  def checkErrorInResponse(response: WSResponse): Unit =
    if (response.status >= 400) {
      throw new Exception(s"""Request failed with status ${response.status} and message "${response.body}"""")
    }

  def getQualifications(queueId: Long, token: String): Future[List[CallQualification]] = {
    xucWs.request(s"call_qualification/queue/$queueId", "GET", token)
      .execute()
      .map { response =>
        checkErrorInResponse(response)
        getResponseFromJson[List[CallQualification]](response)
      }
  }

  def saveQualification(submitData: CallDataSubmit, token: String): Future[Long] = {
    xucWs.request(s"call_qualification", "POST", token)
      .withBody(Json.toJson(submitData))
      .execute()
      .map { response =>
        checkErrorInResponse(response)
        getResponseFromJson[Long](response)
      }
  }
}
