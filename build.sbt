import com.typesafe.sbt.packager.Keys.{dockerBaseImage, dockerCommands, dockerEntrypoint, dockerExposedPorts, dockerExposedVolumes, dockerRepository, maintainer}
import com.typesafe.sbt.packager.docker.{Cmd, DockerPlugin}
import com.typesafe.sbt.web.{CompileProblemsException, GeneralProblem}
import sbt.Path
import sbt.internal.inc.CompileFailed

import scala.sys.process._

val appName = "call-qualification-backend"
val appOrganization = "xivo.solutions"

val appVersion = sys.env.get("TARGET_VERSION").getOrElse("dev-version")

lazy val root = (project in file("."))
  .enablePlugins(PlayScala)
  .enablePlugins(DockerPlugin)
  .enablePlugins(BuildInfoPlugin)
  .settings(
    name := appName,
    version := appVersion,
    buildInfoKeys := BuildInfoKey.ofN(name, version),
    buildInfoPackage := "qualifications.info"
  ).settings(dockerSettings)
  .settings(editsourcesSettings)
  .settings(
    setVersionVarTask := {
      System.setProperty("APPLI_VERSION", appVersion)
    },
    edit in EditSource := ((edit in EditSource) dependsOn (EditSourcePlugin.autoImport.clean in EditSource)).value,
    packageBin in Compile := ((packageBin in Compile) dependsOn (edit in EditSource)).value,
    run in Compile := ((run in Compile) dependsOn setVersionVarTask).evaluated,
    (test in Test) := (test in Test).value,
    (stage in Docker) := (stage in Docker).value
  )

lazy val dockerSettings = Seq(
  maintainer in Docker := "R&D <randd@xivo.solutions>",
  dockerBaseImage := "openjdk:8u162-jdk-slim-stretch",
  dockerExposedPorts := Seq(9000),
  dockerExposedVolumes := Seq("/conf", "/logs"),
  dockerRepository := Some("xivoxc"),
  dockerCommands += Cmd("LABEL", s"""version="${appVersion}""""),
  dockerEntrypoint := Seq("/opt/docker/bin/call_qualification_backend_docker")
)

lazy val editsourcesSettings = Seq(
  flatten in EditSource := true,
  mappings in Universal += file("target/version/appli.version") -> "conf/appli.version",
  targetDirectory in EditSource := baseDirectory(_ / "target/version").value,
  variables in EditSource += version { version => ("SBT_EDIT_APP_VERSION", appVersion) }.value,
  (sources in EditSource) ++= {
    baseDirectory map { bd =>
      (bd / "dist" * "appli.version").get
    }
  }.value
)

lazy val setVersionVarTask = taskKey[Unit]("Set version to a env var")

scalaVersion := "2.12.4"

resolvers ++= Seq(
  "Local Maven Repository" at "file://" + Path.userHome.absolutePath + "/.m2/repository",
  "Typesafe repository" at "https://repo.typesafe.com/typesafe/releases/"
)

libraryDependencies ++= Seq(
  guice,
  jdbc,
  ws,
  ehcache,
  "org.playframework.anorm" %% "anorm" % "2.6.1",
  "org.mockito" % "mockito-all" % "1.9.5",
  "org.postgresql" % "postgresql" % "9.4-1206-jdbc4",
  "org.dbunit" % "dbunit" % "2.4.7",
  "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.2" % Test
)
