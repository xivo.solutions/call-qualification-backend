package controllers.helper

import configuration.AppConfig
import org.mockito.Mockito._
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.mockito.MockitoSugar
import org.scalatestplus.play.PlaySpec
import play.api.libs.ws.{WSClient, WSRequest}
import play.api.mvc.Results

class XucWsSpec extends PlaySpec with Results with MockitoSugar with ScalaFutures {

  class Helper(xucPort: Option[String] = None) {
    val resource = "call_qualification/queue/13"
    val method = "POST"
    val token = "auth-token"
    val host = "xucIp"
    val port = xucPort

    val config: AppConfig = mock[AppConfig]
    stub(config.xucHost).toReturn(host)
    stub(config.xucPort).toReturn(port)

    val wsClient = mock[WSClient]
    val xucWs = new XucWs(wsClient, config)
  }

  "construct XUC URL with port" in new Helper(Some("9009")) {
    xucWs.getXucUrl(resource) mustEqual(s"http://$host:9009/xuc/api/2.0/$resource")
  }

  "construct XUC URL without port" in new Helper(None) {
    xucWs.getXucUrl(resource) mustEqual(s"http://$host/xuc/api/2.0/$resource")
  }

  "construct request" in new Helper() {

    val wsRequest = mock[WSRequest]
    val wsRequestWithToken = mock[WSRequest]
    val wsRequestWithMethod = mock[WSRequest]
    stub(wsClient.url(s"http://$host/xuc/api/2.0/$resource")).toReturn(wsRequest)
    stub(wsRequest.withQueryStringParameters(("token", token))).toReturn(wsRequestWithToken)
    stub(wsRequestWithToken.withMethod(method)).toReturn(wsRequestWithMethod)

    xucWs.request(resource, method, token) mustEqual(wsRequestWithMethod)
  }


}
