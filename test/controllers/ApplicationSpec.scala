package controllers

import akka.stream.Materializer
import controllers.helper.XucWs
import model.CallDataSubmit
import org.scalatest.mockito.MockitoSugar
import org.scalatestplus.play._
import org.scalatestplus.play.guice.GuiceOneAppPerTest
import play.api.libs.json.{JsObject, JsValue, Json}
import play.api.mvc.{AnyContentAsJson, Result}
import play.api.test._
import play.api.test.Helpers._
import org.mockito.Mockito.stub
import play.api.Configuration

import scala.concurrent.Future


class ApplicationSpec extends PlaySpec with GuiceOneAppPerTest with MockitoSugar  {

  class Helper {
    implicit val materializer: Materializer = app.materializer

    val xucWs: XucWs = mock[XucWs]
    val requester: XucRequester = mock[XucRequester]

    val configuration: Configuration = mock[Configuration]
    val controller = new Application(configuration, requester)
    controller.setControllerComponents(Helpers.stubControllerComponents(bodyParser = stubPlayBodyParsers.anyContent))

    val token: String = "token"
    val customData: JsObject = Json.obj(
      "caller"-> "1002",
      "contract" -> "123",
      "optField1" -> "Option"
    )

    val submitData: CallDataSubmit = CallDataSubmit(1, "callid1", Some(""), Some(""), 1, 1, Some("comment"),
  Some(customData), token)
  }

  "save call qualification answer" in new Helper {
    val json: JsValue = Json.obj(
      "sub_qualification_id" -> submitData.subQualificationId,
      "callid" -> submitData.callid,
      "first_name" -> "",
      "last_name" -> "",
      "agent" -> submitData.agentId,
      "queue" -> submitData.agentId,
      "comment" -> submitData.comment,
      "custom_data" -> submitData.customData,
      "token" -> token
    )

    stub(requester.saveQualification(submitData, token)).toReturn(Future.successful(1L))

    val rq: FakeRequest[AnyContentAsJson] = FakeRequest(POST, "/submit").withJsonBody(json)
    val res: Future[Result] = call(controller.submit(), rq)

    status(res) mustBe NO_CONTENT
  }

  "save call qualification answer if name fields are empty" in new Helper {
    val json: JsValue = Json.obj(
      "sub_qualification_id" -> submitData.subQualificationId,
      "callid" -> submitData.callid,
      "agent" -> submitData.agentId,
      "queue" -> submitData.agentId,
      "comment" -> submitData.comment,
      "custom_data" -> submitData.customData,
      "token" -> token
    )

    stub(requester.saveQualification(json.validate[CallDataSubmit].get, token)).toReturn(Future.successful(1L))

    val rq: FakeRequest[AnyContentAsJson] = FakeRequest(POST, "/submit").withJsonBody(json)
    val res: Future[Result] = call(controller.submit(), rq)

    status(res) mustBe NO_CONTENT
  }

  "save call qualification answer with some custom fields" in new Helper {
    override val customData: JsObject = Json.obj(
      "contract" -> "123"
    )

    val json: JsValue = Json.obj(
      "sub_qualification_id" -> submitData.subQualificationId,
      "callid" -> submitData.callid,
      "agent" -> submitData.agentId,
      "queue" -> submitData.agentId,
      "comment" -> submitData.comment,
      "custom_data" -> customData,
      "token" -> token
    )

    stub(requester.saveQualification(json.validate[CallDataSubmit].get, token)).toReturn(Future.successful(1L))

    val rq: FakeRequest[AnyContentAsJson] = FakeRequest(POST, "/submit").withJsonBody(json)
    val res: Future[Result] = call(controller.submit(), rq)

    status(res) mustBe NO_CONTENT
  }

}
