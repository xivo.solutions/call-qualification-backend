package controllers

import org.scalatestplus.play._
import org.scalatestplus.play.guice.GuiceOneAppPerTest
import play.api.test._
import play.api.test.Helpers._

class IndexControllerSpec extends PlaySpec with GuiceOneAppPerTest {

  "IndexController GET" should {

    "render the index page from a new instance of controller" in {
      val controller = app.injector.instanceOf[IndexController]
      val index = controller.index().apply(FakeRequest())

      status(index) mustBe OK
      contentType(index) mustBe Some("text/html")
      contentAsString(index) must include ("Welcome to")
    }

    "render the index page from the application" in {
      val controller = app.injector.instanceOf[IndexController]
      val home = controller.index().apply(FakeRequest())

      status(home) mustBe OK
      contentType(home) mustBe Some("text/html")
      contentAsString(home) must include ("Welcome to")
    }

    "render the index page from the router" in {
      // Need to specify Host header to get through AllowedHostsFilter
      val request = FakeRequest(GET, "/").withHeaders("Host" -> "localhost")
      val home = route(app, request).get

      status(home) mustBe OK
      contentType(home) mustBe Some("text/html")
      contentAsString(home) must include ("Welcome to")
    }
  }
}
