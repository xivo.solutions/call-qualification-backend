package controllers

import model.{CallDataSubmit, CallQualification, SubQualification}
import org.scalatest.mockito.MockitoSugar
import org.scalatestplus.play.PlaySpec
import play.api.Configuration
import play.api.libs.json.{JsValue, Json}
import play.api.libs.ws.{WSRequest, WSResponse}
import play.api.mvc.Results
import play.api.test.Helpers.{INTERNAL_SERVER_ERROR, OK}
import org.mockito.Mockito._

import scala.concurrent.Future
import scala.concurrent.duration._
import akka.util.Timeout
import controllers.helper.XucWs
import org.scalatest.concurrent.ScalaFutures

import scala.util.{Failure, Success}


class XucRequesterSpec extends PlaySpec with Results with MockitoSugar with ScalaFutures {

  class Helper {
    implicit val duration: Timeout = 10.seconds

    val xucWs: XucWs = mock[XucWs]
    val requester: XucRequester = new XucRequester(xucWs)

    val configuration: Configuration = mock[Configuration]
    val token: String = "token"
    val queueId = 1L

    val wsRequest: WSRequest = mock[WSRequest]
    val wsResponse: WSResponse = mock[WSResponse]

    val sq = List(SubQualification(Some(1), "subqualif1"))

    val callQualificationList = List(
      CallQualification(Some(1), "qualif1", sq),
      CallQualification(Some(2), "qualif2", sq),
      CallQualification(Some(3), "qualif3", sq)
    )
  }

  "process json from response" in new Helper {
    val json = Json.toJson(callQualificationList)

    stub(wsResponse.json).toReturn(json)

    requester.getResponseFromJson[List[CallQualification]](wsResponse) mustEqual callQualificationList
  }

  "retrieve call qualifications by queue" in new Helper {
    val json: JsValue = Json.toJson(callQualificationList)

    stub(xucWs.request(s"call_qualification/queue/$queueId", "GET", token)).toReturn(wsRequest)
    stub(wsRequest.execute()).toReturn(Future.successful(wsResponse))
    stub(wsResponse.status).toReturn(OK)
    stub(wsResponse.json).toReturn(json)

    val f: Future[List[CallQualification]] = requester.getQualifications(queueId, token)
    whenReady(f)(res => {
      res mustEqual callQualificationList
      verify(xucWs).request(s"call_qualification/queue/$queueId", "GET", token)
    })
  }

  "save call qualifications" in new Helper {
    val dataToSubmit: CallDataSubmit = CallDataSubmit(1, "123.456", Some("first"), Some("last"), 1, 1, None, None, token)
    val json: JsValue = Json.toJson(1L)

    stub(xucWs.request(s"call_qualification", "POST", token)).toReturn(wsRequest)
    stub(wsRequest.withBody(Json.toJson(dataToSubmit))).toReturn(wsRequest)
    stub(wsRequest.execute()).toReturn(Future.successful(wsResponse))
    stub(wsResponse.status).toReturn(OK)
    stub(wsResponse.json).toReturn(json)

    val f: Future[Long] = requester.saveQualification(dataToSubmit, token)
    whenReady(f)(res => {
      res mustEqual 1L
      verify(xucWs).request(s"call_qualification", "POST", token)
    })
  }

  "return an error if get qualification response from xuc server is invalid" in new Helper {
    val json: JsValue = Json.toJson(1L)

    stub(xucWs.request(s"call_qualification/queue/$queueId", "GET", token)).toReturn(wsRequest)
    stub(wsRequest.execute()).toReturn(Future.successful(wsResponse))
    stub(wsResponse.status).toReturn(INTERNAL_SERVER_ERROR)
    stub(wsResponse.body).toReturn("failed")
    stub(wsResponse.json).toReturn(json)

    val f: Future[List[CallQualification]] = requester.getQualifications(queueId, token)
    import scala.concurrent.ExecutionContext.Implicits.global
    f onComplete {
      case Failure(e) => succeed
      case Success(_) => fail()
    }
  }

  "return an error if save qualification response from xuc server is invalid" in new Helper {
    val dataToSubmit: CallDataSubmit = CallDataSubmit(1, "123.456", Some("first"), Some("last"), 1, 1, None, None, token)
    val json: JsValue = Json.toJson(1L)

    stub(xucWs.request(s"call_qualification", "POST", token)).toReturn(wsRequest)
    stub(wsRequest.withBody(Json.toJson(dataToSubmit))).toReturn(wsRequest)
    stub(wsRequest.execute()).toReturn(Future.successful(wsResponse))
    stub(wsResponse.status).toReturn(INTERNAL_SERVER_ERROR)
    stub(wsResponse.body).toReturn("failed")
    stub(wsResponse.json).toReturn(json)

    val f: Future[Long] = requester.saveQualification(dataToSubmit, token)
    import scala.concurrent.ExecutionContext.Implicits.global
    f onComplete {
      case Failure(e) => succeed
      case Success(_) => fail()
    }
  }

  "return an error if response from xuc server is invalid" in new Helper {
    a[Exception] should be thrownBy  {
      stub(wsResponse.status).toReturn(INTERNAL_SERVER_ERROR)
      requester.checkErrorInResponse(wsResponse)
    }
  }

  "return an error with unparsable JSON" in new Helper {
    stub(wsResponse.json).toReturn(Json.parse("{}"))

    a[Exception] should be thrownBy {
      requester.getResponseFromJson[List[CallQualification]](wsResponse)
    }
  }
}
