package controllers

import akka.stream.Materializer
import model._
import org.scalatest.mockito.MockitoSugar
import org.scalatestplus.play.PlaySpec
import play.api.Configuration
import play.api.libs.ws.WSClient
import play.api.test.{FakeRequest, Helpers}
import play.api.test.Helpers.{BAD_REQUEST, GET, INTERNAL_SERVER_ERROR, NOT_FOUND, OK, POST, contentAsString, contentType, status, stubPlayBodyParsers}
import org.mockito.Mockito._

import scala.concurrent.Future
import scala.concurrent.duration._
import akka.util.Timeout
import controllers.helper.XucWs
import org.scalatestplus.play.guice.GuiceOneAppPerTest
import play.api.libs.json._

class XucThirdPartySpec extends PlaySpec with GuiceOneAppPerTest with MockitoSugar {

  class Helper {
    implicit val materializer: Materializer = app.materializer
    implicit val duration: Timeout = 10.seconds

    val wsClient: WSClient = mock[WSClient]
    val configuration: Configuration = mock[Configuration]
    val requester: XucRequester = mock[XucRequester]
    val xucWs: XucWs = mock[XucWs]
    val wsUrl = "ws://localhost"
    val token: String = "token"

    stub(configuration.get[String]("baseurl")).toReturn("localhost")
    stub(configuration.get[String]("frontEndBaseUrl")).toReturn("localhost")
    stub(configuration.get[String]("xuc.host")).toReturn("localhost")
    stub(configuration.get[String]("xuc.port")).toReturn("9900")
    stub(configuration.getOptional[String]("edge.host")).toReturn(Some("localhost"))
    stub(configuration.getOptional[String]("autopauseReason")).toReturn(Some("testReason"))
    stub(configuration.getOptional[String]("autopauseReasonPerQueue")).toReturn(None)
    stub(configuration.getOptional[String]("event")).toReturn(Some("EventEstablished"))

    val controller = new XucThirdParty(configuration, requester, xucWs)

    controller.setControllerComponents(Helpers.stubControllerComponents(bodyParser = stubPlayBodyParsers.anyContent))

    val sq = List(SubQualification(Some(1), "subqualif1"))

    val callQualificationList = List(
      CallQualification(Some(1), "qualif1", sq),
      CallQualification(Some(2), "qualif2", sq),
      CallQualification(Some(3), "qualif3", sq)
    )

    val expectedResponseUrl = Some("localhost" +
      "/qualification?queueId=" + 1 +
      "&token=" + "token" +
      "&caller=" + "caller" +
      "&callee=" + "calledNumber" +
      "&agentId=" + 1L +
      "&queueId=" + 1L +
      "&callId=" + "callid" +
      "&xucUrl=localhost"
    )

    val requestPayload: JsValue = Json.parse(
      s"""{"user":{"userId":5,"agentId":1,"firstName":"User","lastName":"Four","fullName":"User Four",
          "token":"token"},
          "callee":"callee","caller":"caller","userData":{"XIVO_USERID":"5","USR_XIVO_DSTNUM":"calledNumber"},
          "callDataCallId":"callid","queue":{"id":1,"name":"queue","displayName":"Queue","number":"1012"}}
    """
    )
  }

  "create on call url" in new Helper {
    val userData: Map[String, String] = Map("USR_XIVO_DSTNUM" -> "calledNumber")
    val user: WsUser = WsUser(1L, 1L, "first", "last", "first last", "token")
    val queue: WsQueue = WsQueue(1L, "queue", "Queue", "1000")
    val req: WsRequest = WsRequest("calledNumber", "caller", user, queue, Some(userData), "callid")

    controller.createOnCallUrl(req) mustEqual expectedResponseUrl
  }

  "return an on call response with url" in new Helper {
    val queueId: Long = 1L

    stub(requester.getQualifications(queueId, token)).toReturn(Future.successful(callQualificationList))

    val qualification = controller.onCall.apply(FakeRequest(POST, "/thirdparty/ws").withJsonBody(requestPayload))

    status(qualification) mustBe OK
    contentType(qualification) mustBe Some("application/json")
    val content = Json.parse(contentAsString(qualification))
    content\("title") mustEqual(JsDefined(JsString("Qualification")))
    content\("autopause") mustEqual(JsDefined(JsBoolean(true)))
    content\("autopauseReason") mustEqual(JsDefined(JsString("testReason")))
    content\("event") mustEqual(JsDefined(JsString("EventEstablished")))
    content\("url") mustEqual(JsDefined(JsString(expectedResponseUrl.get)))
  }


  "return 404 if the queue has no qualifications assigned" in new Helper {
    val queueId: Long = 1L
    stub(requester.getQualifications(queueId, token)).toReturn(Future.successful(List()))

    val qualification = controller.onCall.apply(FakeRequest(POST, "/thirdparty/ws").withJsonBody(requestPayload))

    status(qualification) mustBe NOT_FOUND
  }

  "gets the correct pause if multiples reasons are configured" in new Helper {
    val queueId: Long = 1L

    stub(configuration.getOptional[String]("autopauseReason")).toReturn(None)
    stub(configuration.getOptional[String]("autopauseReasonPerQueue")).toReturn(Some("{\"queueA\":\"pauseA\", \"queueB\":\"pauseB\"}"))
    stub(requester.getQualifications(queueId, token)).toReturn(Future.successful(callQualificationList))

    val requestQueueA: JsValue = Json.parse(
      s"""{"user":{"userId":5,"agentId":1,"firstName":"User","lastName":"Four","fullName":"User Four",
          "token":"token"},
          "callee":"callee","caller":"caller","userData":{"XIVO_USERID":"5","USR_XIVO_DSTNUM":"calledNumber"},
          "callDataCallId":"callid","queue":{"id":1,"name":"queueB","displayName":"Queue B","number":"1012"}}
    """
    )

    val qualification = controller.onCall.apply(FakeRequest(POST, "/thirdparty/ws").withJsonBody(requestQueueA))
    val content = Json.parse(contentAsString(qualification))

    content\("autopauseReason") mustEqual(JsDefined(JsString("pauseB")))
  }

  "gets the correct pause if multiples reasons are configured and a default pause is set as fallback" in new Helper {
    val queueId: Long = 1L

    stub(configuration.getOptional[String]("autopauseReason")).toReturn(Some("autopaused"))
    stub(configuration.getOptional[String]("autopauseReasonPerQueue")).toReturn(Some("{\"queueA\":\"pauseA\", \"queueB\":\"pauseB\"}"))
    stub(requester.getQualifications(queueId, token)).toReturn(Future.successful(callQualificationList))

    val requestQueueA: JsValue = Json.parse(
      s"""{"user":{"userId":5,"agentId":1,"firstName":"User","lastName":"Four","fullName":"User Four",
          "token":"token"},
          "callee":"callee","caller":"caller","userData":{"XIVO_USERID":"5","USR_XIVO_DSTNUM":"calledNumber"},
          "callDataCallId":"callid","queue":{"id":1,"name":"queueB","displayName":"Queue B","number":"1012"}}
    """
    )

    val qualification = controller.onCall.apply(FakeRequest(POST, "/thirdparty/ws").withJsonBody(requestQueueA))
    val content = Json.parse(contentAsString(qualification))

    content\("autopauseReason") mustEqual(JsDefined(JsString("pauseB")))
  }

  "post-call pause is false in API response when no pause is configured" in new Helper {
    val queueId: Long = 1L

    stub(configuration.getOptional[String]("autopauseReason")).toReturn(None)
    stub(configuration.getOptional[String]("autopauseReasonPerQueue")).toReturn(None)
    stub(requester.getQualifications(queueId, token)).toReturn(Future.successful(callQualificationList))

    val requestQueueA: JsValue = Json.parse(
      s"""{"user":{"userId":5,"agentId":1,"firstName":"User","lastName":"Four","fullName":"User Four",
          "token":"token"},
          "callee":"callee","caller":"caller","userData":{"XIVO_USERID":"5","USR_XIVO_DSTNUM":"calledNumber"},
          "callDataCallId":"callid","queue":{"id":1,"name":"queueB","displayName":"Queue B","number":"1012"}}
    """
    )

    val qualification = controller.onCall.apply(FakeRequest(POST, "/thirdparty/ws").withJsonBody(requestQueueA))
    val content = Json.parse(contentAsString(qualification))

    content\("autopause") mustEqual(JsDefined(JsBoolean(false)))
  }

  "gets the autopauseReason value if the queue is missing from the json value" in new Helper {
    val queueId: Long = 1L

    stub(configuration.getOptional[String]("autopauseReason")).toReturn(Some("autopaused"))
    stub(configuration.getOptional[String]("autopauseReasonPerQueue")).toReturn(Some("{\"queueA\":\"pauseA\", \"queueB\":\"pauseB\"}"))
    stub(requester.getQualifications(queueId, token)).toReturn(Future.successful(callQualificationList))

    val requestQueueA: JsValue = Json.parse(
      s"""{"user":{"userId":5,"agentId":1,"firstName":"User","lastName":"Four","fullName":"User Four",
          "token":"token"},
          "callee":"callee","caller":"caller","userData":{"XIVO_USERID":"5","USR_XIVO_DSTNUM":"calledNumber"},
          "callDataCallId":"callid","queue":{"id":1,"name":"queueC","displayName":"Queue C","number":"1012"}}
    """
    )

    val qualification = controller.onCall.apply(FakeRequest(POST, "/thirdparty/ws").withJsonBody(requestQueueA))
    val content = Json.parse(contentAsString(qualification))

    content\("autopauseReason") mustEqual(JsDefined(JsString("autopaused")))
  }

}
