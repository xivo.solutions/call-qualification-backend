package models

import model.{CallData, CallDataSubmit}
import org.joda.time.DateTime
import org.scalatestplus.play.PlaySpec
import play.api.libs.json._
import org.scalatest.mockito.MockitoSugar

class CallDataSpec extends PlaySpec with MockitoSugar {

  "CallData" should {
    "convert to json" in {
      val cd = CallData("caller1", "callee1", 1, 1, "callid.1", "token")

      Json.toJson(cd) mustEqual Json.obj(
        "caller" -> cd.caller,
        "callee" -> cd.callee,
        "agentId" -> cd.agentId,
        "queueId" -> cd.queueId,
        "callId" -> cd.callId,
        "token" -> cd.token
      )
    }
  }

  "CallDataSubmit" should {
    "convert to json" in {
      val customData: JsObject = Json.obj(
        "caller"-> "1002",
        "contract" -> "123",
        "optField1" -> "Option"
      )
      val cd = CallDataSubmit(1, "callid.1", Some("User"), Some(""), 1, 1, Some("comment"), Some(customData), "token")

      val result = Json.toJson(cd).toString

      result must include("\"sub_qualification_id\":1")
      result must include("time")
      result must include(DateTime.now().toString("yyyy-MM-dd"))
      result must include("\"callid\":\"callid.1\"")
      result must include("\"agent\":1")
      result must include("\"queue\":1")
      result must include("\"first_name\":\"User\"")
      result must include("\"last_name\":\"\"")
      result must include("\"comment\":\"comment\"")
      result must include("\"custom_data\":\"{\\\"caller\\\":\\\"1002\\\",\\\"contract\\\":\\\"123\\\",\\\"optField1\\\":\\\"Option\\\"}\"")
    }

    "be created from json" in {
      val customData: JsObject = Json.obj(
        "caller"-> "1002",
        "contract" -> "123",
        "optField1" -> "Option"
      )

      val json: JsValue = Json.parse(
        s"""{"sub_qualification_id":1,"callid":"callid.1","agent":1,"queue":1,
          |"first_name":"User","last_name":"","comment":"comment","custom_data":${customData.toString}, "token": "token"}"""
          .stripMargin)

      val cd = CallDataSubmit(1, "callid.1", Some("User"), Some(""), 1, 1, Some("comment"), Some(customData), "token")

      json.validate[CallDataSubmit] match {
        case JsSuccess(res, _) => res mustEqual cd
        case JsError(errors) =>
          fail()
      }
    }
  }
}