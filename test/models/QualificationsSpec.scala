package models

import model.{CallQualification, SubQualification}
import org.scalatestplus.play.PlaySpec
import play.api.libs.json.{JsError, JsSuccess, JsValue, Json}

class QualificationsSpec extends PlaySpec {

  "CallQualifications" should {
    "convert to json" in {
      val sq = List(SubQualification(Some(1), "subqualif1"))
      val q = CallQualification(Some(1), "qualif1", sq)

      Json.toJson(q) mustEqual Json.obj(
        "id" -> q.id,
        "name" -> q.name,
        "subQualifications" -> sq
      )
    }

    "be created from json" in {
      val json: JsValue = Json.parse(
        """{"id": 1, "name": "qualif1",
          | "subQualifications": [{"id": 1, "name": "subqualif1"}, {"id": 2, "name": "subqualif2"}]}""".stripMargin)

      val sq = List(SubQualification(Some(1), "subqualif1"), SubQualification(Some(2), "subqualif2"))
      val qualif = CallQualification(Some(1), "qualif1", sq)

      json.validate[CallQualification] match {
        case JsSuccess(q, _) => q mustEqual qualif
        case JsError(errors) => fail()
      }
    }
  }
}